# 卒業論文: プログラミング教育サービス - Rabbit(CAN)

## Rabbit(CAN)とは

> 世界最高水準の IT 社会の実現に向けて、平成 25 年 6 月に「日本再興戦略 -JAPAN is BACK-」が
閣議決定された。産業競争力の源泉となるハイレベルな IT 人材の育成・確保を目的に、平成 26 年度か
ら、産学官連携による実践的 IT 人材を継続的に育成するための仕組みを構築し、義務教育段階からの
プログラミング等の IT 教育を推進するという。義務教育段階のプログラミング教育に関しては、平成
24 年度から実施されている中学校学習指導要領において、技術・家庭科の教育内容が改訂され、技術
分野内容「D 情報に関する技術」では、プログラムによる計測・制御が必修とされている。情報化の進
展の中で、情報に関する技術が多くの産業を支えるとともに、社会生活や家庭生活を変化させてきたこ
とを理解し、より良い社会を築くために情報に関する技術を適切に評価し活用する能力と態度を育成す
るのが大きな目的だ。他方、平成 23 年 4 月に文部科学省から公表された「教育の情報化ビジョン」の
もと、21 世紀にふさわしい学びと学校の創造を目指して、情報活用の実践力、情報の科学的な理解、
情報社会に参画する態度などを育成する情報教育の充実、教科指導における情報通信技術の活用、校務
の情報化なども推進されてきている。
情報化、グローバル化が急速に進む知識基盤社会にあって、未来を担い 21 世紀を生き抜く児童・生
徒のための、上記のような教育は、日本のみならず先進諸国で推し進められてきている。

[諸外国における プログラミング教育に関する調査研究より引用](http://jouhouka.mext.go.jp/school/pdf/programming_syogaikoku_houkokusyo.pdf)

> TODO:プログラミング教育等のの推移グラフ

プログラミング教育等の教育現場への導入は〇〇年を境に多く取り組まれ始めている

このように今日学校教育の場において国を上げての積極的なプログラミング等の教育が重視されつつある  
我々はこの点に着目し、また開発者個人の持つプログラミング経験に基づき、初期のプログラミング教育  
において重要視される基礎的部分や、躓きやすい部分を徹底的に配慮した新しいプログラミング学習サービス  
を提供し、この論文においては実際にプログラミング初学者に実際に使ってもらい、  
理解度の推移や、データの基づく考察などを行っていく。  

## 問題点
日本でも2020年度より小学校で必修化されるなど取組が強化される予定だが、  
現状の問題点として、日本国内の教育現場においてのコンピューターの普及率の低さである  
現状1台あたりの生徒数が全国平均で6.2人であり、1台のコンピューターのCPUコア数より  
人間の数のほうが多いのが現状である。全国の教育機関において  これを改善するために総務省は  
2020年までに1人1台のパソコン普及を目指している。

![教育用コンピュータ１台当たりの児童生徒数全国](https://s3-ap-northeast-1.amazonaws.com/private-photo-strage/Screenshot+from+2018-06-14+12-23-16.png)

[平成27年度学校における教育の情報化の実態等に関する調査結果（概要）より引用](http://www.mext.go.jp/component/a_menu/education/micro_detail/__icsFiles/afieldfile/2016/10/13/1376818_1.pdf)

## なぜ作るのか
今日、プログラミング必修化が進むに連れプログラミングを教える教室やイベントが多く開催されるようになった  
それに伴い質の高いプログラミング学習を求めるほど、金銭面において高額な費用が必要に成る。  
実際に足を運びメンターと呼ばれる講師にプログラミングを少人数で教えてもらえるスクールなどは、
月1万円ほどの講習費がかかってしまい、子供にプログラミング学習をさせてあげたくても難しい環境である現状がある。  
[プログラミングスクール料金より引用](https://blog.codecamp.jp/programming-study-school)  

![プログラミング講座団体推移](https://static.techacademy.jp/magazine/wp-content/uploads/2016/06/629278a18cc88191da6b8faeeb566743-768x452.png)  
[2020年から小学校でプログラミング教育が必修化へ！今までの流れをまとめてみたより引用](https://techacademy.jp/magazine/8525)  

そこで自宅学習でなおかつ無償で使える、そしてAIなどを使いつまづきを解析や警告、教育現場では同じミスをしている生徒の通知など  
プログラマだからわかる目線でしっかりとした基礎を積むことのできるサービスを作成しようと言うことで作ることになりました  


## 作成
> [ソースコード](https://gitlab.com/advancing-life)

### 使用言語及びフレームワーク
- WebFlont:          [Vue.js(JavaScript)](https://jp.vuejs.org/)
- WebBack:           [Sinatra(Ruby)](http://sinatrarb.com/)
- WebBackDB:         [Postgresql](https://www.postgresql.org/)
- Middleware:        [Echo(Golang)](https://echo.labstack.com/)
- DesktopApp: [Electron(JavaScript)](https://electronjs.org/)
- SmartPhoneApp:     [VueNative(JavaScript)](https://vue-native.io/)

### 開発環境
- Docker
- AWS
- GitLab/GitHub
- GitLab/CI


### Developers

- [Meltens](https://github.com/Meltens)
- [Flmil](https://github.com/flmil)

### 構図
![構図1](https://raw.githubusercontent.com/advancing-life/rabbit-can-/master/.github/infra.jpg)
![構図2](https://s3-ap-northeast-1.amazonaws.com/private-photo-strage/%E3%82%82%E3%81%A3%E3%81%8F.svg)


### 概要
ユーザーは、クライアント(Web,Desktop,スマートフォン)からアクセスすることができ、Loginなどの情報は  
Sinatraを中間としたサーバーが、PostgresSQLを管理して、ユーザー情報や、保持しているファイルデータ、  
所属校などの管理をするAPIになる。  

またリアルな実行環境を再現するためにGolang製の軽量フレームワークEchoを使いクライアント側と非同期通信をし、  
Echoはユーザーがプロジェクト作成画面に入った時点で、そのユーザー専用のDockerContainerを作成  
実際のコマンドなどをクライアント側が非同期でEchoに送り、EchoがDockerに指示を出し仮想環境上で、  
Debian/Bashを動かすことによって、よりリアルな、CUIクライアントの動きが可能にしている　　

### 私の担当と実行環境
このプロジェクトで実際に私が携わる部分はMiddleware(コンソール部分)でユーザーが快適にBashなどを使うことのできるように整備することが求められます

#### Echo(Middleware)までの動き

1. まずユーザー側はサーバー(EC2)にアクセスしマークアップ(Vuejs)を読み込みます
2. 次に、ユーザーはログインないしサインアップを試みます
3. サインアップなどのデータはEchoは保持していないためSinatra側にリクエストされます。
4. Sinatra側が認証をセッションなどを確立します
5. ユーザー側はSinatraから受け取ったデータを下に自分の所属グループなどを選択します
6. そして、自分の環境に入る選択をして始めてEcho側にリクエストを投げます

##### Echo(Middleware)の動き
まずMiddlewareの動きを以下の図のようにまとめました

![Echo1](https://github.com/advancing-life/rabbit-can-middleware/blob/master/.images/Middleware-1.jpg?raw=true)
![Echo2](https://github.com/advancing-life/rabbit-can-middleware/blob/master/.images/Middleware-2.jpg?raw=true)

1 クライアント側が自分の環境はいる選択をするとEcho側にWebSocketコネクション用のURLを発行するためのリクエストをします　
　
> http://hostname.com/api/v1/connection/:lang (GET)

2 次にリクエストを受け取ったEchoはEcho自体を実行している環境上でDockerコマンドを実行しDebianに使用する言語を内蔵したコンテナを立ち上げる指示を出します  

> Rubyコンテナを立ち上げるための実際のコマンド
~~~sh
$ docker run --name コンテナID -itd　jpnlavender/ruby_on_debian /bin/bash
~~~

3 コマンドを受け取ったDockerはコンテナを作りUniqIDをEcho側に返却します  
4 それを受け取ったEchoはNoSqlのRedisにEcho側が作成したコンテナ名（乱数）とDocker側が作成したコンテナIDをKeyValueとして保存します  

> Echo内で実際に実行されるRedisへの保存関数
~~~golang
func SET(key string, value string) (err error) {
	r, err := Init()
	if err != nil {
		return
	}
	_, err = r.Do("SET", key, value)
	if err != nil {
		return
	}
	return
}
~~~

5 そして保存したことを検証した後問題なく作成されていた場合はクライアント側にコンテナIDを返却します

> 実際にクライアントに返却されるJSONデータ
 ~~~json
{
    "url": "ws://localhost:1234/api/v1/execution_environment/ce16824e6180167ef65b1803c6b21b5d",
    "container_id": "ce16824e6180167ef65b1803c6b21b5d",
    "result": "99f7f325eea9e42d7c494e6fc9a69e778b5a071dbba5504ebc6072147a8a9323 "
}
~~~

6 クライアントは受け取ったコンテナIDを使用しEchoのWebSocket用のプロトコルにコネクションを張るようにリクエストします

> リクエストするURL
ws://hostname.com/api/v1/socket/:container_id 

7 指示を受け取ったEchoはクライアントとWebSocketコネクションを張ります。

##### ここまでがコネクションを張るまでのEchoの動きですそして、コネクションを張った後の動きが以下のようになります

1 コネクションを張ったクライアントはコマンド入力が面においてBashコマンドを入力します

> クライアントがクライアントの画面にて入力する例
~~~sh
user@container_id $ ls
~~~

2 入力されたコマンドを非同期通信でEchoに気味の悪い拡張子JSON形式で受け渡します

> クライアント側がEchoに渡すJSONデータ
~~~json
{
    "container_id": "11aa1d97274d794a90fe10c32ba828de",
    "command": "ls",
}
~~~

3 jsonを受け取ったEchoは、受け取ったコンテナIDを元にDockerを起動しコマンドを実行させます

>　実行されるEcho内の関数
```golang
func Exec(execChan chan ExecutionCommand, execmd ExecutionCommand, name string) {
	defer close(execChan)
	c, err := shellwords.Parse("docker exec -i " + name + " sh -c '" + execmd.Command + "'")
	if err != nil {
		execmd.ErrResult = err
		execChan <- execmd
	}
	cmd := exec.Command(c[0], c[1:]...)
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		execmd.ErrResult = err
		execChan <- execmd
	}
	stderr, err := cmd.StderrPipe()
	if err != nil {
		execmd.ErrResult = err
		execChan <- execmd
	}
	err = cmd.Start()
	if err != nil {
		execmd.ErrResult = err
		execChan <- execmd
	}
	streamReader := func(scanner *bufio.Scanner, outputChan chan string, doneChan chan bool) {
		defer close(outputChan)
		defer close(doneChan)
		for scanner.Scan() {
			outputChan <- scanner.Text()
		}
		doneChan <- true
	}
	stdoutScanner := bufio.NewScanner(stdout)
	stdoutOutputChan := make(chan string)
	stdoutDoneChan := make(chan bool)
	stderrScanner := bufio.NewScanner(stderr)
	stderrOutputChan := make(chan string)
	stderrDoneChan := make(chan bool)
	go streamReader(stdoutScanner, stdoutOutputChan, stdoutDoneChan)
	go streamReader(stderrScanner, stderrOutputChan, stderrDoneChan)
	stillGoing := true
	for stillGoing {
		select {
		case <-stdoutDoneChan:
			stillGoing = false
		case line := <-stdoutOutputChan:
			execmd.Result = line
			execChan <- execmd
		case line := <-stderrOutputChan:
			execmd.Result = line
			execChan <- execmd
		}
	}
	ret := cmd.Wait()
	if ret != nil {
		execmd.ErrResult = err
		execChan <- execmd
	}
}
```

4 実行されている間のコマンドの結果は一行ずつ読み込まれ一行ずつクライアント側に非同期通信で返却されます

> 返却されるJSONデータ
~~~json
{
    "container_id": "11aa1d97274d794a90fe10c32ba828de",
    "command": "ls",
    "result":"bin/n",
}
~~~

# 今後の予定
そして開発環境にDocker, GitLab/CIを使うことによって、テストの自動化、開発環境の統一しデプロイなどもEC2に自動化する予定です
